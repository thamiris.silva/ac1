from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/media', methods=['POST'])
def calcular_media():
    valores = request.json['valores']
    soma = sum(valores)
    media = soma / len(valores)
    return jsonify({'media': media})

if __name__ == '__main__':
    app.run(debug=True)


#testar a rota

#http://localhost:5000/media